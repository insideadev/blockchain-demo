const {Blockchain, Transaction} = require('./blockchain');
const EC = require('elliptic').ec;
const ec = new EC('secp256k1');

const myKey = ec.keyFromPrivate('5a39603d955ab7c7bfc3a645e71196488359a977305ea0077bafc4a4ed488d9e');
const myWalletAddress = myKey.getPublic('hex');

let myChain = new Blockchain();

const tx = new Transaction(myWalletAddress, 'Some address', 10);
tx.signTransaction(myKey);
myChain.addTransaction(tx);
myChain.minePendingTransactions(myWalletAddress);

console.log('Kien\'s balance: ', myChain.getBalanceOfAddress(myWalletAddress));
console.log('Blockchain: ', JSON.stringify(myChain, null, 4));

myChain.chain[1].transactions[0].amount = 100;
console.log('Is chain valid: ', myChain.isValid());
