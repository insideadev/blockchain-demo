// const promise = new Promise((resolve, reject) => {
//     setTimeout(() => {
//         resolve(('data back from the server'));
//     }, 1500);
//     setTimeout(() => {
//         reject(('there was an error'));
//     }, 1000);
// });

// promise.then(response => console.log(response))
//     .catch(error => console.log(error));

// let user = {
//     name: "Kien",
//     age: 27,
//     address: {
//         country: "Vietnam",
//         city: "Hanoi"
//     }
// };

// const { name, age, address: { city } } = user;
// console.log(`Name: ${name}, Age: ${age}, City: ${city}`);

// Generator
// let gen = function* (numbers) {
//     for (let i = 0; i < numbers.length; i++) {
//         yield numbers[i];
//     }
// };

// const genNumbers = gen([1, 2, 3, 4]);

// const interval = setInterval(() => {
//     let next = genNumbers.next();
//     if (next.done) {
//         console.log('This generator is done');
//         clearInterval(interval);
//     } else {
//         console.log(next.value);
//     }
// }, 1000);


const greeting = theName => {
    const greeting =  new Promise((resolve, reject) => {
        setTimeout(() => reject('hello ' + theName), 2000);
    });
    greeting.then(data => console.log(data));
    return greeting;
}


const greeting2 = async name => {
    const greeting = new Promise((resolve, reject) => {
        setTimeout(() => resolve('hello ' + name), 2000);
    });
    console.log(await greeting);
    return greeting;
}

const test = async () => {
   try {
    const g1 = await greeting('Kien');

   } catch (error) {
       console.log(error);
   }
}


greeting2('Kien2');
test();



